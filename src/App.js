import './App.css';
import MultipleCounter from "./components/MultipleCounter";

function App() {
    return (
        <div className="App flex items-center justify-center min-h-screen">
            <MultipleCounter/>
        </div>
    );
}

export default App;
