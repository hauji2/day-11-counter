import {useSelector} from "react-redux";

const CounterGroupSum = () => {
    const sum = useSelector((state) => state.counter.sum);
    return (
        <div className="text-white m-4 text-2xl">Sum: {sum}</div>
    )
}

export default CounterGroupSum;