import {useEffect, useState} from "react";
import {Button} from "./Button";
import {useDispatch, useSelector} from "react-redux";
import {updateSum} from "../app/counterSlice";

export const Counter = () => {
    const size = useSelector((state) => state.counter.size);
    const sum = useSelector((state) => state.counter.sum);
    const dispatch = useDispatch();

    const [counterNumber, setCounterNumber] = useState(0);

    useEffect(() => {
        setCounterNumber(0)
    }, [size]);

    const handleAddClick = () => {
        setCounterNumber(counterNumber + 1);
        dispatch(updateSum(sum + 1));
    };

    const handleSubClick = () => {
        setCounterNumber(counterNumber - 1);
        dispatch(updateSum(sum - 1));
    };

    return (
        <div className="flex flex-row gap-x-6 mb-0.5">
            <Button logo={"+"} handleClick={handleAddClick}/>
            <div className="text-white text-2xl w-12">{counterNumber}</div>
            <Button logo={"-"} handleClick={handleSubClick}/>
        </div>
    )
}