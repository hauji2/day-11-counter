import CounterSizeGenerator from "./CounterSizeGenerator";
import CounterGroupSum from "./CounterGroupSum";
import {CounterGroup} from "./CounterGroup";
import {useDispatch} from "react-redux";
import {updateSize, updateSum} from "../app/counterSlice";

let MultipleCounter = () => {
    const dispatch = useDispatch()

    const handleSizeChange = (event) => {
        dispatch(updateSize(event.target.value));
        dispatch(updateSum(0));
    }

    return (
        <div>
            <CounterSizeGenerator handleSizeChange={handleSizeChange}/>
            <CounterGroupSum/>
            <CounterGroup/>
        </div>
    );
}

export default MultipleCounter;