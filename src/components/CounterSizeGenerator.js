import {useSelector} from "react-redux";

const CounterSizeGenerator = ({handleSizeChange}) => {
    const size = useSelector((state) => state.counter.size)
    return (
        <div className="border border-white p-5 m-4">
            <label className="text-white text-2xl" htmlFor="size">size:</label>
            <input type="text" name="size" value={size} onChange={handleSizeChange}/>
        </div>
    )
}

export default CounterSizeGenerator;