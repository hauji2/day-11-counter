import {Counter} from "./Counter";
import {useSelector} from "react-redux";

export const CounterGroup = () => {
    const size = useSelector((state) => state.counter.size);

    return (
        <div className="m-4 flex flex-col justify-center items-center">
            {Array.from({length: size}, (_, i) => <Counter key={i}/>)}
        </div>
    )
}