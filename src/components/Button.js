export const Button = ({handleClick, logo}) => {
    return (
        <button className="bg-gray-50 border-0 rounded w-9 text-2xl h-11 font-bold"
                onClick={handleClick}>
            {logo}
        </button>
    )
}