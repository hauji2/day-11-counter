import {createSlice} from '@reduxjs/toolkit'

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        size: 0,
        sum: 0
    },
    reducers: {
        updateSize: (state, action) => {
            state.size = action.payload
        },
        updateSum: (state, action) => {
            state.sum = action.payload
        }
    },
})

export const {updateSize, updateSum} = counterSlice.actions

export default counterSlice.reducer